# Robbie Online

To run this yourself you'll need some files I can't provide because those are owned by Ambrasoft.

You'll need to:
- Put your `Robbie2D.swf` in `static/flash/`
- Add `Robbie2D.xml` to `worlds/` and register at `worlds_data.csv`
- Put all the tile-textures in `static/tiles`
- Get Russle from [ruffle.rs](https://ruffle.rs/), extract the zip in `static/` and point to it in `show_world.html`  
  (`ruffle_nightly_2021_02_03_selfhosted` confirmed to work)