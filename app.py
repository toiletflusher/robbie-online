from flask import Flask, render_template, send_from_directory, session, request
from os import environ, listdir
import csv
import re
app = Flask(__name__)

app.secret_key = environ.get('SESSIONKEY', 'secret')
default_world = 'origineel'
worlds = {}


def load_worlds():
    with open('worlds_data.csv') as f:
        global worlds
        worlds = {
            list(row.values())[0]: {k: v for k, v in row.items()}
            for row in csv.DictReader(f, skipinitialspace=True)
            if "" not in row.values() and len(row.values()) == 3
        }


def read_world(world_name):
    load_worlds()
    with open(f"worlds/world_{worlds[world_name]['world_id']}.xml") as file:
        return file.read()


def update_csv():
    with open('worlds_data.csv', 'w') as file:
        dict_writer = csv.DictWriter(file, list(worlds.values())[0].keys())
        dict_writer.writeheader()
        dict_writer.writerows(worlds.values())


@app.route('/world/<world_name>')
def player_page(world_name):
    load_worlds()
    if world_name in worlds:
        session['world'] = world_name
        return render_template('show_world.html', world_name=world_name)
    else:
        return render_template('world_notfound.html')


@app.route('/')
def home_page():
    return player_page(default_world)


@app.route('/static/<path:path>')
def send_flash(path):
    return send_from_directory('static', path)


def valid_xml(xml_string: str) -> bool:
    try:
        xml_split = xml_string.split('\n')
        xml_split = [x for x in xml_split if x != '']
        assert xml_split[0] == '<RobbieMaps>'
        assert xml_split[-1] == '</RobbieMaps>'
        for level_string in xml_split[1:-1]:
            assert level_string[:20] == '    <RobbieMap map="'
            assert level_string[-3:] == '"/>'
            actual_data = level_string[20:-3]
            assert len(actual_data) == 216
        return True
    except AssertionError:
        return False
    except IndexError:
        return False


@app.route('/add_world', methods=['POST'])
def add_world():
    load_worlds()
    world_data = request.values.get('world_data')
    if not valid_xml(world_data):
        return 'XML test failed: De geleverde level data voldeed niet aan de eisen', 406

    world_name = request.values.get('world_name')
    world_name = re.sub(r'[^a-z0-9\s-]', '', world_name.lower()).replace(' ', '-')
    version_name = world_name
    version = 0
    while version_name in worlds:
        version += 1
        version_name = f"{world_name}_v{version}"
    world_name = version_name

    if len(world_name) > 128:
        return 'Wereld naam te lang, maximaal 128 tekens', 406

    next_file_id = max([int(world_dict['world_id']) for world_dict in worlds.values()]) + 1
    world_file = f"worlds/world_{next_file_id}.xml"
    worlds[world_name] = {'name': world_name, 'votes': 1, 'world_id': next_file_id}
    with open(world_file, 'w') as file:
        file.write(world_data)
    update_csv()

    return world_name, 200


@app.route('/editor/<world_name>')
def world_editor(world_name):
    load_worlds()
    if world_name in worlds:
        return render_template('world_editor.html', world_data=read_world(world_name), tile_ids=list_tile_ids())
    else:
        return render_template('world_notfound.html')


@app.route('/worlds_menu')
def worlds_menu():
    load_worlds()
    worlds_info = [(world['name'], world['votes']) for world in worlds.values()]
    worlds_info.sort(key=lambda x: int(x[1]), reverse=True)
    return render_template('worlds_menu.html', worlds_info=worlds_info)


@app.route('/Robbie2D.xml')
@app.route('/world/Robbie2D.xml')
def send_world():
    """Request from flash"""
    load_worlds()
    return read_world(session['world'])


@app.route('/vote', methods=['POST'])
def vote_up():
    load_worlds()
    world_name = request.values.get('world_name')
    direction = request.values.get('direction')
    worlds[world_name]['votes'] = int(worlds[world_name]['votes'])
    if direction == 'up':
        worlds[world_name]['votes'] += 1
    else:
        worlds[world_name]['votes'] -= 1
    update_csv()
    return '', 204


def list_tile_ids() -> list:
    return [x.split('.')[0] for x in listdir('static/tiles') if x[-4:] == '.png']


@app.route('/contact')
def contact():
    return render_template('contact.html')


load_worlds()
if __name__ == '__main__':
    port = int(environ.get("PORT", 5000))
    app.run(host='0.0.0.0', port=port, debug=True)
